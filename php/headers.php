<?php

  session_start();
  if($_SESSION==null){
    header("Location: /eracomseminario");
  }
  
  // tipo de respuesta y aceptar peticion de cualquier tipo
  header("Content-type: application/json");
  header("Access-Control-Allow-Origin: *");

  // Debugear código
  error_reporting(E_ALL);
  ini_set('display_errors','On');

?>
