<?php

require '../vendor/autoload.php';  

$conexion = new MongoDB\Client("mongodb://localhost:27017");  

?>

<div class="container">
	<br>
	<h1>Prueba</h1>
	<br>
	<h4>Nivel Intermedio</h4>
	<table class="table table-sm">
		<tr>
			<th>No</th>
			<th>Pregunta</th>
			<th>Respuesta</th>
		</tr>
		<?php
			//Generar aleatorios
			$newgenerate=0;
			for ($i=1; $i <16 ; $i++) { 
				$numbergenerate=rand(1,20);
				//$idgenerate[$i]=$numbergenerate;
		 	 	
		 	 	if ($i==1){
		 	 		$idgenerate[$i]=$numbergenerate;
		 	 		//echo $numbergenerate." <br>";
		 	 	}else{
		 	 		for ($x=1; $x <$i ; $x++) { 
		 	 			//echo $idgenerate[$x]." == ".$numbergenerate."<br>";
		 	 			if ($newgenerate==1) {
		 	 				$x=1;
		 	 				$newgenerate=0;
		 	 			}

			 	 		while ($idgenerate[$x] == $numbergenerate) {
			 	 			$numbergenerate=rand(1,20);
			 	 			$newgenerate=1;
			 	 		} 	 			
		 	 		}
		 	 		$idgenerate[$i]=$numbergenerate;
		 	 		//echo $numbergenerate." <br>";
		 	 	}
			}

			//Ciclo para mostrar 15 preguntas
			$db = $conexion->prueba3;
			$col = $db->nivel2;
			for ($x=1; $x <16 ; $x++) { 
			//}
				//$db = $conexion->prueba3;
				//$col = $db->nivel1;
				$row = $col->find( [ 'idpregunta' =>$idgenerate[$x] ]);

			    foreach ($row as $question) {
			
					?>
					<tr>
						<td><?php echo "".$x; ?></td>
						<td><?php echo "".$question['descripcion']; ?> </td>
						<td>
							<?php
							echo "<select id='cmbAnswer".$x."' class='custom-select custom-select-sm' id='inputGroupSelect01'>";
					            
					              echo " <option value='0'>Elija una opcion...</option> ";
					              echo " <option value='".$question['op1']."' >".$question['op1']."</option> ";
					              echo " <option value='".$question['op2']."' >".$question['op2']."</option> ";
					              echo " <option value='".$question['op3']."' >".$question['op3']."</option> ";
					              echo " <option value='".$question['op4']."' >".$question['op4']."</option> ";
					              //echo " <option value='1' >".$obj['correcta']."</option> ";
					              //<option value="1">+</option>
					            
					            //<option value="0">Elija una opcion</option>
					            ?>
					            
			         		 </select>
						</td>
					</tr>
		<?php
			}
		}
		?>
		<tr>
			<td colspan="2">
				<button class="btn btn-primary btn-sm" onclick="sendanswer();">Evaluar respuestas</button>        	
			</td>
		</tr>
	</table>
</div>