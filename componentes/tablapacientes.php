
<?php 
	session_start();
	require '../vendor/autoload.php';  

	$conexion = new MongoDB\Client("mongodb://localhost:27017");
 ?>
	<div class="row" >
		<div class="col-sm-12">
			<br>
			<h3><center>Puntuaciones</center></h3>
			<br>
			<table class="table table-sm table-hover table-responsive-sm">
				<thead class="thead-dark">
					<tr >
						<th>No</th>
						<th>Nombre</th>
						<th>Apellido</th>
						<th><center>Total preguntas</center></th>
						<th>Respuestas correctas</th>
						<th><center>Respuestas incorrectas</center></th>
						<th><center>Punteo total</center></th>

					</tr>
				</thead>
				<tbody>
				<?php
					$db = $conexion->prueba3;
					//$page = isset($_GET['p'])?$_GET['p']:'';
					$collection = $db->punteo;
					$total=$collection->count();
					$rowscore = $collection->find();

					$x=0;
					foreach ($rowscore as $datacollecion) {
					    // hacer algo a cada documento
				    	$dataresult[$x][0]=$datacollecion['totalpreguntas'];
				    	$dataresult[$x][1]=$datacollecion['totalcorrectas'];
				    	$dataresult[$x][2]=$datacollecion['totalincorrectas'];
				    	$dataresult[$x][3]=$datacollecion['punteototal'];
				    	//$datapunteo[$x][4]=$datacollecion['estudiante_id'];

					    $db = $conexion->prueba3;  
					    //Creación de documento 
					    $coleccion = $db->estudiante; 
					    $rowstudent = $coleccion->find( [ 'idestudiante' =>$datacollecion['estudiante_id'] ]);
					    
					    foreach ($rowstudent as $student) {  
					      $datapunteo[$x][4] =$student['nombre'];
					      $datapunteo[$x][5] =$student['apellido'];

					    }
					    
					    $x=($x+1);    
					//}
					    //$number=($x+1);
					}


					for ($i=0; $i <$total ; $i++) { 
						for ($j=0; $j <$total ; $j++) { 
							if ($dataresult[$i][3]>$dataresult[$j][3]) {

								$datatemp[0][0]=$dataresult[$j][0];
								$datatemp[0][1]=$dataresult[$j][1];
								$datatemp[0][2]=$dataresult[$j][2];
								$datatemp[0][3]=$dataresult[$j][3];
								$datatemp[0][4]=$datapunteo[$j][4];
								$datatemp[0][5]=$datapunteo[$j][5];

								$dataresult[$j][0]=$dataresult[$i][0];
								$dataresult[$j][1]=$dataresult[$i][1];
								$dataresult[$j][2]=$dataresult[$i][2];
								$dataresult[$j][3]=$dataresult[$i][3];
								$datapunteo[$j][4]=$datapunteo[$i][4];
								$datapunteo[$j][5]=$datapunteo[$i][5];

								$dataresult[$i][0]=$datatemp[0][0];
								$dataresult[$i][1]=$datatemp[0][1];
								$dataresult[$i][2]=$datatemp[0][2];
								$dataresult[$i][3]=$datatemp[0][3];
								$datapunteo[$i][4]=$datatemp[0][4];
								$datapunteo[$i][5]=$datatemp[0][5];
							}
						}
					}
					$number=0;
					for ($x=0; $x <$total ; $x++) { 
						$number=($x+1);
			  echo "<tr>
						<td nowrap>".$number."</td>
						<td nowrap>".$datapunteo[$x][4]."</td>
						<td nowrap>".$datapunteo[$x][5]."</td>
						<td nowrap>".$dataresult[$x][0]."</td>
						<td nowrap>".$dataresult[$x][1]."</td>
						<td nowrap>".$dataresult[$x][2]."</td>
						<td nowrap>".$dataresult[$x][3]."</td>
					</tr>";
					 	//$x=($x+1);	
						}
					 ?>
				 </tbody>
			</table>
		</div>
	</div>

<!-- Modal detalles-->
<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="ModalAllDataPatient" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header"> 
        <center><h5 class="modal-title" id="exampleModalLabel">Datos</h5></center>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table table-sm">
        	<tbody>
        		<tr>
        			<td>
        				<input type="text" id="nameEdit" class="form-control form-control-sm" type="text" placeholder="Nombre">	
        			</td>
        			<td>
        				<input type="date" id="fechaEdit" class="form-control form-control-sm" type="text" placeholder="Fecha">	
        			</td>
        			<td>
        				<input type="text" id="profesionEdit" class="form-control form-control-sm" placeholder="Profesion">
        			</td>
        		</tr>
        		<tr>
        			<td>
        				<input type="text" id="escolaridadEdit" class="form-control form-control-sm" type="text" placeholder="Escolaridad">	
        			</td>
        			<td>
        				<input type="text" id="originarioEdit" class="form-control form-control-sm" type="text" placeholder="Originario">	
        			</td>
        			<td>
        				<input type="text" id="residenciaEdit" class="form-control form-control-sm" placeholder="Residencia">
        			</td>
        		</tr>
        		<tr>
        			<td>
        				<input type="text" id="religionEdit" class="form-control form-control-sm" type="text" placeholder="Religion">	
        			</td>
        			<td>
        				<input type="number" id="dpiEdit" class="form-control form-control-sm" type="text" placeholder="DPI">	
        			</td>
        			<td>
        				<input type="text" id="fuentinfEdit" class="form-control form-control-sm" placeholder="Fuente de informacion">
        			</td>
        		</tr>
        		<tr>
        			<td>
        				<input type="number" id="hijosEdit" class="form-control form-control-sm" type="text" placeholder="Hijos">	
        			</td>
        			<td>
        				<input type="number" id="hvEdit" class="form-control form-control-sm" type="text" placeholder="Hijos vivos">	
        			</td>
        			<td>
        				<input type="phone" id="hmEdit"class="form-control form-control-sm"  placeholder="Hijos muertos">
        			</td>
        		</tr>
        		<tr>
        			<td>
        				<input type="text" id="nomemergEdit" class="form-control form-control-sm" type="text" placeholder="Nombre de emergencia">	
        			</td>
        			<td>
        				<input type="number" id="telemergEdit" class="form-control form-control-sm" type="text" placeholder="Tel. Emergencia">	
        			</td>
        			<td>
        				<input type="text" id="entrevistadoEdit" class="form-control form-control-sm"  placeholder="Entrevistado por:">
        			</td>
        		</tr>
        	</tbody>
        		
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-warning">Actualizar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="ModalMedicament" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header"> 
        <center><h5 class="modal-title" id="exampleModalLabel">Medicina</h5></center>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		  <div id="combomedicina">
				
		  </div>
		  </br>

		  <div class="form-group" id="grupnNomMedicina">
		    <label for="lmedicina" id="labmedicina">Medicina</label>
		    <input type="text" id="medicina" placeholder="nombre" required="true" class="form-control form-control-sm">
		  </div>
		  <div class="form-group">
		    <label for="lcant">Cantidad</label>
		    <input type="number" id="cantmed" value="" placeholder="cantidad" required="true" class="form-control form-control-sm">
		  </div>
		  <div class="form-group">
		    <label for="lcant">Paciente</label>
		    <input type="text" readonly=”readonly” id="pacientemed" value="" placeholder="paciente" required="true" class="form-control form-control-sm">
		  </div>
		  <input type="text" id="idpaci_med" hidden="true">
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cerrar</button>
        <button id="btnsave" class="btn btn-success btn-sm" onclick="insermedicament()"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
        <button id="btnupdate" class="btn btn-warning btn-sm" onclick="updatemedicament()" data-dismiss="modal">Actualizar</button>
      </div>
    </div>
  </div>
</div>

