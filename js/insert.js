function newQuestions(){
  var question=$('#newquestion').val();
  var option1=$('#op1').val();
  var option2=$('#op2').val();
  var option3=$('#op3').val();
  var option4=$('#op4').val();
  var answer=$('#answerquestion').val();
  var nivel =$("#Nivel").val();

  var dataquestion={question: question,
                    option1: option1,
                    option2: option2,
                    option3: option3,
                    option4: option4,
                    answer: answer,
                    nivel: nivel}
  $.ajax({
    url: "php/insertQuestions.php",
    type: "POST",
    dataType: "JSON",
    data: dataquestion,
    success:function(response){
      console.log(response);
      if (response == 1) {
        //cleardataregister(); //LImpia los cmapos de registros
        $('#admincontenedor').load('componentes/regpregunstas.html');
        alertify.success("Datos registrados");
      }else{
        alertify.error("Debe llenar todos los campos");
      }

    },
    error: function(e){
        //alertify.success("Datos registrados");
        //loadNewpatient();
    }
  });

}

function sendanswer(){
  // var arreglo = new Array();
  var answer = new Array(15)
  var idquestion = new Array(15);
  var truAns = new Array(15);

  var x=0, score=0;
  var answertrue=0, answerwrong=0, allquestions=0;

  for (x = 1; x < 15; x++) {
    answer[x]=$('#cmbAnswer'+x).val();
    idquestion[x]=$('#questionid'+x).val(); 
    truAns[x]=$('#trueAns'+x).val();

    if (answer[x] == truAns[x]) {
      answertrue=answertrue+1;
    }else{
      answerwrong=answerwrong+1;
    }


/*
    var dataquestions1 = {
      'answer'+x: $('#cmbAnswer'+x).val(),
      'idquestion'+x: $('#questionid'+x).val(),
      //data_account: "true"
    }
*/
  }

  allquestions=(answertrue+answerwrong);
  score=(answertrue*6.66667);

  var dataAsnwer = {
      correctas: answertrue,
      incorrectas: answerwrong,
      total: allquestions,
      puntuacion: score
    }

  $.ajax({
    url: "php/insertpuntuacion.php",
    type: "POST",
    dataType: "JSON",
    data: dataAsnwer,
    success:function(response){
      console.log(response);
      if (response == 1) {
        //cleardataregister(); //LImpia los cmapos de registros
        $('#contenedor').load('componentes/tablapacientes.php');
        alertify.success("Datos registrados :)");
      }else{
        alertify.error("Debe llenar todos los campos");
      }

    },
    error: function(e){
        //alertify.success("Datos registrados");
        //loadNewpatient();
    }
  });

}

function insertStudent(tipuser){
      var dataStuden = {
      nombre: $("#nombre").val(),
      apellido: $("#apellido").val(),
      genero: $("#genero").val(),
      direccion: $("#direccion").val(),
      telefono: $("#telefono").val(),
      email: $("#email").val(),
      user: $("#user").val(),
      pass: $("#pass").val(),
      tip: tipuser,
      //username: nombre[0] + apellido,
      data_account: "true"
    }

    $.ajax({
      url: "php/insertar.php",
      type: "POST",
      dataType: "JSON",
      data: dataStuden,
      success:function(response){
        console.log(response);
        if (response == 1) {
          cleardataregister(); //LImpia los cmapos de registros
          alertify.success("Datos registrados :)");
        }else{
          alertify.error("Debe llenar todos los campos");
        }

      },
      error: function(e){
          //alertify.success("Datos registrados");
          //loadNewpatient();
      }
    });
}

function cleardataregister(){
    $("#nombre").val('');
    $("#apellido").val('');
    $("#genero").val('');
    $("#direccion").val('');
    $("#telefono").val('');
    $("#email").val('');
    $("#user").val('');
    $("#pass").val('');
}


//funcion insertar medicamento
  function insermedicament(){
    var a;
    var datosmedicament = {
      medicina: $("#medicina").val(),
      cantmed: $("#cantmed").val(),
      patient: $("#idpaci_med").val(),
      data_account: "true"
    }

    $.ajax({
      url: "php/regmedicament.php",
      type: "POST",
      dataType: "JSON",
      data: datosmedicament,
      success:function(response){
        alert(response);
        if (response.register == true) {
          alertify.success("Datos registrados ");
        }else{
          console.log("Vuelva a intentarlo ");
        }

      },
      error: function(e){
        alertify.success("Datos registrados ");
      }
    });
  }


  function alerthola(){
    alert("hola");
  }







    $("#formRegPaciente").submit(function(){

    // usuario se registra con la primera letra y Apellido
    /*
    var nombre = $("#nombre").val();
    var apellido = $("#apellido").val();
    */
    var datosPaciente = {
      nombre: $("#nombre").val(),
      fnacimiento: $("#fnacimiento").val(),
      genero: $("#genero").val(),
      estado: $("#estado").val(),
      profesion: $("#profesion").val(),
      escolaridad: $("#escolaridad").val(),
      originario: $("#originario").val(),
      residencia: $("#residencia").val(),
      religion: $("#religion").val(),
      finfo: $("#finfo").val(),
      dpi:$("#dpi").val(),
      gsanguineo: $("#gsanguineo").val(),
      hv: $("#hv").val(),
      hm: $("#hm").val(),
      nomemerg: $("#nomemerg").val(),
      telemerg: $("#telemerg").val(),
      vivecon: $("#vivecon").val(),//nombre del encargado
      telencargado: $("#telencargado").val(),
      parentesco: $("#parentesco").val(),
      entrevistador: $("#entrevistador").val(),
      tipificacion: $("#tipificacion").val(),
      funcional: $("#funcional").val(),
      caida: $("#caida").val(),
      ulcera: $("#ulcera").val(),
      nutricion: $("#nutricion").val(),
      mental: $("#mental").val(),
      afectivo: $("#afectivo").val(),
      odontologico: $("#odontologico").val(),
      quiropedia: $("#quiropedia").val(),
      //username: nombre[0] + apellido,
      data_account: "true"
    }

    /*
      alert(datosPaciente.nombre);
      alert(datosPaciente.fnacimiento);

      */
      //alert(datosPaciente.quiropedia);

    $.ajax({
      url: "php/regpaciente.php",
      type: "POST",
      dataType: "JSON",
      data: datosPaciente,
      success:function(response){
        console.log(response);
        if (response.registerDentist == true) {
          $("#btn-Rreset").click(); //btn en form para resetear los valores
          //$('#NuevoDentista').modal('toggle');
          //getDentistas(); //Actualizar la lista de Dentistas
          alertify.success("Datos registrados :)");
        }else{
          console.log("Vuelva a intentarlo ");
        }

      },
      error: function(e){
          alertify.success("Datos registrados");
          loadNewpatient();
      }
    });


    return false;
  });
